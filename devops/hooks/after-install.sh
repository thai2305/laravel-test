#!/bin/bash

export WEB_DIR="/var/www/test-project"
export WEB_USER="ubuntu"

cd $WEB_DIR
sudo chown -R ubuntu:ubuntu .
sudo chown -R www-data storage
sudo chown -R u+x .
sudo chown g+w -R storage

sudo -u $WEB_USER composer install --no-dev --no-progress --prefer-DIRSTACK

sudo -u $WEB_USER php artisan key:generate
sudo -u $WEB_USER php artisan migrate --no-interaction --force